This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Application Functionality

Todo app in react, redux with material UI:

- Setup the application base using `create-react-app` which provide default linting tool(eslint) for code format.
- Implemented Add new todo task and mark as completed facility.
- User can filter the list using bottom quick action bar. (FYI, Authentication is not implemented)
- While click on task's delete or edit. It will not render the whole list. Prevented the siblings render
- Deployed the app on google cloud
- https://todo-react-284513.uc.r.appspot.com/
- app.yaml file is used for the cloud configuration
- Test cases are generated using the `react-test-renderer`.

## Available Scripts

### `npm install`

To install packages

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

### Application Snapshot

![Application snapshot](screenshots/all-task.png)
![Application snapshot](screenshots/active-task.png)
![Application snapshot](screenshots/completed-task.png)
![Application snapshot](screenshots/mobile-view.png)
