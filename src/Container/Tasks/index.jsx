import React, { useState, useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Typography,
  Box,
  makeStyles,
  CircularProgress,
} from "@material-ui/core";
import { displayDataType, STATUS_CODE } from "Helpers/Constants";
import QuickActionBar from "Components/Tasks/QuickActionBar";
import TaskItem from "Components/Tasks/TaskItem";
import AddTaskInput from "Components/Tasks/AddTaskInput";
import API from "API/API";
import { ACTION_TYPE } from "Redux/Task/Actions";

const useStyles = makeStyles((theme) => ({
  appContainer: {
    margin: "0 auto",
  },
  titleContainer: {
    textAlign: "center",
  },
  inputContainer: {
    textAlign: "center",
    margin: theme.spacing(2),
  },

  noDataFound: {
    textAlign: "center",
    margin: theme.spacing(2),
  },
  taskWrapper: {
    border: "1px solid #ccc",
    borderRadius: theme.spacing(2),
    backgroundColor: "#e2e2e2",
    boxShadow: theme.shadows[1],
    margin: theme.spacing(2),
  },
  taskContainer: {
    padding: theme.spacing(1),
  },
  progressContainer: {
    borderRadius: theme.spacing(2),
    backgroundColor: "#e2e2e2",
    padding: theme.spacing(15, 2),
    margin: theme.spacing(2),
    textAlign: "center",
  },
}));

const api = new API();
/**
 * Tasks Container component
 */
export default function Tasks() {
  const classes = useStyles();
  const dispatch = useDispatch();

  // Get data from redux
  const taskList = useSelector((state) => state.Task.taskList);

  const [isBusy, setIsBusy] = useState(true);
  const [filterTaskList, setFilterTaskList] = useState([]);
  const [taskListFilterType, setTaskListFilterType] = useState(
    displayDataType.ALL
  );

  useEffect(() => {
    api.get("task/list").then((response) => {
      if (response.status === STATUS_CODE.SUCCESS) {
        setIsBusy(false);
        dispatch({
          type: ACTION_TYPE.FETCH_DATA,
          taskList: response.data,
        });
      }
    });
  }, [dispatch]);

  const handleTaskListFilter = useCallback(
    (type) => {
      setTaskListFilterType(type);
      let filterTaskListData = [];
      if (type === displayDataType.ACTIVE) {
        filterTaskListData = taskList.filter((task) => !task.is_completed);
      } else if (type === displayDataType.COMPLETED) {
        filterTaskListData = taskList.filter((task) => task.is_completed);
      }
      setFilterTaskList(filterTaskListData);
    },
    [taskList]
  );

  // Add new task API call
  const addNewTask = useCallback(
    (task) => {
      if (task !== "") {
        api
          .post("task/add", {
            data: {
              task,
            },
          })
          .then((response) => {
            if (response.status === STATUS_CODE.CREATED) {
              dispatch({
                type: ACTION_TYPE.FETCH_DATA,
                taskList: [response.data, ...taskList],
              });
            }
          });
      }
    },
    [taskList, dispatch]
  );

  // Complete API call
  const completeTask = useCallback(
    (taskData) => {
      api
        .put("task/edit", {
          data: {
            id: taskData.id,
            is_completed: true,
          },
        })
        .then((response) => {
          if (response.status === STATUS_CODE.UPDATED) {
            dispatch({
              type: ACTION_TYPE.EDIT_TASK,
              taskData,
            });
          }
        });
    },
    [dispatch]
  );

  // Delete task API call
  const deleteTask = useCallback(
    (taskData) => {
      api.delete(`task/delete/${taskData.id}`).then((response) => {
        if (response.status === STATUS_CODE.SUCCESS) {
          dispatch({
            type: ACTION_TYPE.DELTE_TASK,
            taskData: taskData,
          });
        }
      });
    },
    [dispatch]
  );

  const taskDisplayData =
    taskListFilterType === displayDataType.ALL ? taskList : filterTaskList;
  return (
    <Box className={classes.appContainer}>
      <Box className={classes.titleContainer}>
        <Typography variant="h3" gutterBottom>
          Todos
        </Typography>
      </Box>
      <Box className={classes.inputContainer}>
        <AddTaskInput addNewTask={addNewTask} />
      </Box>
      {isBusy ? (
        <Box className={classes.progressContainer}>
          <CircularProgress />
        </Box>
      ) : (
        <Box className={classes.taskWrapper}>
          {taskDisplayData && taskDisplayData.length > 0 ? (
            <React.Fragment>
              <Box className={classes.taskContainer}>
                {taskDisplayData.map((taskData) => (
                  <TaskItem
                    key={`el-${taskData.id}`}
                    taskData={taskData}
                    completeTask={completeTask}
                    deleteTask={deleteTask}
                  />
                ))}
              </Box>
            </React.Fragment>
          ) : (
            <Box className={classes.noDataFound}> No data found.</Box>
          )}
          <QuickActionBar
            taskListFilterType={taskListFilterType}
            taskLength={taskList.length}
            handleTaskListFilter={handleTaskListFilter}
          />
        </Box>
      )}
    </Box>
  );
}
