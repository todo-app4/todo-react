import React from "react";
import Tasks from "./Tasks";
/**
 * Base Component / Dashboard
 */
export default function BaseContainer() {
  return (
    <React.Fragment>
      <Tasks />
    </React.Fragment>
  );
}
