export const APP_PATH =
  process.env.NODE_ENV === "production" ? process.env.REACT_APP_BASE_PATH : "";

export const BASE_URL = "https://todo-node-28441-6aaqqpo7fq-uc.a.run.app/";

export const STATUS_CODE = {
  INTERNAL_SERVER_ERROR: 500,
  SUCCESS: 200,
  CREATED: 201,
  UPDATED: 201,
};

export const displayDataType = {
  ALL: "ALL",
  COMPLETED: "COMPLETED",
  ACTIVE: "ACTIVE",
};

export const THEME_COLOR = {
  primaryColor: "",
  primaryColorDark: "",
  primaryColorLight: "",
};
