import Task from "Redux/Task/Reducers";
import { combineReducers } from "redux";

const rootReducer = combineReducers({
  Task,
});

export default rootReducer;
