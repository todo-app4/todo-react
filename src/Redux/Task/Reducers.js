import { ACTION_TYPE } from "./Actions";

const initialState = {
  taskList: [],
};

/**
 * Task Reducer
 */

const TaskReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_TYPE.FETCH_DATA:
      return {
        ...state,
        taskList: action.taskList,
      };
    case ACTION_TYPE.EDIT_TASK:
      return {
        ...state,
        taskList: editTask(state.taskList, action.taskData),
      };
    case ACTION_TYPE.DELTE_TASK:
      return {
        ...state,
        taskList: deleteTask(state.taskList, action.taskData),
      };
    default:
      return state;
  }
};

export default TaskReducer;

function deleteTask(taskList, taskData) {
  let taskListData = [...taskList];
  const taskIndex = taskListData.findIndex((task) => task.id === taskData.id);
  if (taskIndex !== -1) {
    taskListData.splice(taskIndex, 1);
  }
  return taskListData;
}

function editTask(taskList, taskData) {
  let taskListData = [...taskList];
  const taskIndex = taskListData.findIndex((task) => task.id === taskData.id);
  if (taskIndex !== -1) {
    let task = taskListData[taskIndex];
    task.is_completed = true;
    taskListData[taskIndex] = task;
  }
  return taskListData;
}
