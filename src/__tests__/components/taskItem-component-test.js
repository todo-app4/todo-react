import React from "react";
import { Provider } from "react-redux";
import TaskItem from "Components/Tasks/TaskItem";
import renderer from "react-test-renderer";
import { store } from "Redux/Store";
import taskData from "fixture/data.json";

describe("TaskItem Component", () => {
  it("should render without throwing an error", async () => {
    const rendered = renderer.create(
      <Provider dispatch={jest.fn} store={store}>
        <TaskItem
          taskData={taskData}
          deleteTask={jest.fn}
          completeTask={jest.fn}
        />
      </Provider>
    );
    expect(rendered.toJSON()).toMatchSnapshot();
  });
});
