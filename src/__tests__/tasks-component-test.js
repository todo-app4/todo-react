import React from "react";
import { Provider } from "react-redux";
import Tasks from "Container/Tasks";
import renderer from "react-test-renderer";
import { store } from "../Redux/Store";

describe("Tasks Component", () => {
  it("should render without throwing an error", async () => {
    const rendered = renderer.create(
      <Provider dispatch={jest.fn} store={store}>
        <Tasks />
      </Provider>
    );
    expect(rendered.toJSON()).toMatchSnapshot();
  });
});
