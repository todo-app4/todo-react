import React, { useState } from "react";
import { IconButton, makeStyles, TextField } from "@material-ui/core";
import { Send } from "@material-ui/icons";
import PropTypes from "prop-types";

const useStyles = makeStyles((theme) => ({
  input: {
    width: "50%",
    borderRadius: theme.spacing(5),
  },
}));

/**
 * AddTaskInput compoent
 */

function AddTaskInput({ addNewTask }) {
  const classes = useStyles();
  const [inputValue, setInputValue] = useState("");

  const setValueInState = (e) => {
    setInputValue(e.target.value);
  };

  const keypressHandler = (e) => {
    if (e.key === "Enter") {
      addNewTask(e.target.value);
      clearInputValue();
    }
  };

  const clearInputValue = (e) => {
    setInputValue("");
  };

  return (
    <React.Fragment>
      {console.log("AddTaskInput rendered")}
      <TextField
        value={inputValue}
        className={classes.input}
        id="outlined-basic"
        label="Add new task"
        variant="outlined"
        onChange={setValueInState}
        onKeyPress={keypressHandler}
      />
      <IconButton
        color="primary"
        component="span"
        onClick={() => {
          addNewTask(inputValue);
          clearInputValue();
        }}
      >
        <Send />
      </IconButton>
    </React.Fragment>
  );
}

AddTaskInput.propTypes = {
  addNewTask: PropTypes.func,
};

export default React.memo(AddTaskInput);
