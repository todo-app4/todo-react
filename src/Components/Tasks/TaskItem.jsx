import React, { useState, useCallback } from "react";
import { Done, Delete } from "@material-ui/icons";
import { Box, IconButton, makeStyles, Typography } from "@material-ui/core";
import PropTypes from "prop-types";

const useStyles = makeStyles((theme) => ({
  task: {
    padding: theme.spacing(1, 2),
    marginBottom: theme.spacing(1),
    backgroundColor: "#fff",
    border: "1px solid #ccc",
    borderRadius: theme.spacing(5),
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    boxShadow: theme.shadows[1],
  },
}));

/**
 * Task Item compoent
 */

function TaskItem({ taskData, deleteTask, completeTask }) {
  const classes = useStyles();
  const [taskItem, setTaskItem] = useState(taskData);
  const [isDelete, setIsdelete] = useState(false);

  const handleCompleteTask = useCallback(() => {
    taskItem.is_completed = true;
    setTaskItem({ ...taskItem });
    completeTask(taskItem);
  }, [completeTask, taskItem]);

  const handleDeleteTask = useCallback(() => {
    deleteTask(taskItem);
    setIsdelete(true);
  }, [deleteTask, taskItem]);

  return (
    <React.Fragment>
      {console.log("TaskItem Rendered Component =>", taskItem.task)}
      {!isDelete && (
        <Box className={classes.task}>
          <Typography>{taskItem.task}</Typography>
          <Box className={classes.actionContainer}>
            {!taskItem.is_completed && (
              <IconButton
                color="primary"
                component="span"
                onClick={handleCompleteTask}
              >
                <Done fontSize="small" />
              </IconButton>
            )}
            <IconButton
              color="secondary"
              component="span"
              onClick={handleDeleteTask}
            >
              <Delete fontSize="small" />
            </IconButton>
          </Box>
        </Box>
      )}
    </React.Fragment>
  );
}

const comparisonFn = (prevProps, nextProps) => {
  return prevProps.taskData === nextProps.taskData;
};

TaskItem.propTypes = {
  taskData: PropTypes.object,
  deleteTask: PropTypes.func,
  completeTask: PropTypes.func,
};

export default React.memo(TaskItem, comparisonFn);
