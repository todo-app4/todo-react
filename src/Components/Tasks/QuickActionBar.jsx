import React from "react";
import { useSelector } from "react-redux";
import { Button, Typography, Box, makeStyles } from "@material-ui/core";
import { displayDataType } from "Helpers/Constants";
import PropTypes from "prop-types";

const useStyles = makeStyles((theme) => ({
  quickActionContaier: {
    textAlign: "center",
    margin: theme.spacing(1),
    display: "flex",
  },
  itemLeft: {
    width: "80px",
    float: "left",
  },
  buttonContainer: {
    width: "calc(100% - 80px)",
  },
  button: {
    margin: `${theme.spacing(0.5)}px`,
    fontWeight: 700,
  },
}));

/**
 * Quick ActionBar component
 */

function QuickActionBar({
  taskLength,
  taskListFilterType,
  handleTaskListFilter,
}) {
  const classes = useStyles();
  const taskList = useSelector((state) => state.Task.taskList);
  return (
    <Box className={classes.quickActionContaier}>
      {console.log("QuickActionBar rendered for update count")}
      <Box className={classes.itemLeft}>
        <Typography>ALL({taskList.length})</Typography>
      </Box>
      <Box className={classes.buttonContainer}>
        <Button
          className={classes.button}
          size="small"
          color={`${
            displayDataType.ALL === taskListFilterType ? "primary" : "default"
          }`}
          onClick={() => handleTaskListFilter(displayDataType.ALL)}
        >
          All
        </Button>
        <Button
          className={classes.button}
          size="small"
          color={`${
            displayDataType.ACTIVE === taskListFilterType
              ? "primary"
              : "default"
          }`}
          onClick={() => handleTaskListFilter(displayDataType.ACTIVE)}
        >
          Active
        </Button>
        <Button
          className={classes.button}
          size="small"
          color={`${
            displayDataType.COMPLETED === taskListFilterType
              ? "primary"
              : "default"
          }`}
          onClick={() => handleTaskListFilter(displayDataType.COMPLETED)}
        >
          Completed
        </Button>
      </Box>
    </Box>
  );
}

QuickActionBar.propTypes = {
  taskLength: PropTypes.number,
  handleTaskListFilter: PropTypes.func,
  taskListFilterType: PropTypes.string,
};

export default React.memo(QuickActionBar);
